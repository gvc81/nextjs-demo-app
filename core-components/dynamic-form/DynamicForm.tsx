import React from "react";
import {Input, Form, Button} from 'antd'
import FormBuilder from 'antd-form-builder'
import {modelDefFields} from "../../gvc-meta-data-lib/src";

function fieldDefsToAntdFormComponentMapper(fieldDefs) {
  return fieldDefs.map(fieldDef => {
    // console.log("finding widget for fielddef",fieldDef);
    let widget = fieldDefsToAntDComponentMap[fieldDef.type];
    if (!widget) {
      throwComponentNotYetSupportedError(fieldDef.type);
    }

    let antDField: any = {
      label: fieldDef.label,
      widget,
      key: fieldDef.name
    };

    if (fieldDef.type === "boolean") {
      // Convert string boolean in to actual boolean
      antDField = {
        ...antDField,
        getInitialValue: (field, initialValues, form) => {
          // console.log("init field value", initialValues);
          let initialValue = initialValues[field.key];
          return initialValue === "true" ? true : false;
        }
      }
    }
    return antDField;
  });
};

function throwComponentNotYetSupportedError(componentName) {
  throw new Error(`Component '${componentName}' not yet supported`);
}

function generateAntDFormBuilderMeta(fieldDefs) {
  return {
    fields: fieldDefsToAntdFormComponentMapper(fieldDefs)
  }
}

const fieldDefsToAntDComponentMap = {
  string: "input",
  text: "textarea",
  date: "date-picker",
  boolean: "checkbox",
  combo: "select",
  password: "password",
  int: "number",
  float: "number",
  drop_down: "select",
  datatable: false,
  derived: false,
};

const options = ['Apple', 'Orange', 'Banana'];

const DynamicForm = Form.create()(({form, initialValues, isEdit}) => {
  // @ts-ignore
  /*const {modelDefId} = router.query;
  const modelDef = modelDefsData.find(modelDef => modelDef._id === modelDefId);
  console.log("router", modelDef);*/

  const handleSubmit = evt => {
    evt.preventDefault();
    // @ts-ignore
    console.log('submit: ', form.getFieldsValue())
  };

  const newMeta = generateAntDFormBuilderMeta(modelDefFields);
  // console.log("new meta", JSON.stringify(newMeta, null, 2));

  return (
    <Form onSubmit={handleSubmit}>
      <FormBuilder meta={newMeta} form={form} initialValues={initialValues}/>
      <Form.Item wrapperCol={{span: 16, offset: 8}}>
        <Button htmlType="submit" type="primary">
          {isEdit ? "Update" : "Create"}
        </Button>
      </Form.Item>
    </Form>
  )
});

export default DynamicForm;
