import Link from 'next/link';
import {Button, Popconfirm, Table} from 'antd';
import React, {Component} from "react";
import "./dynamic-table-styles.css";
import {fieldDefsToAntdTableColumnMapper} from "../utils/DynamicTableUtils";

// @ts-ignore
class DynamicTable extends Component<any, any> {
  constructor(props) {
    super(props);
    const {dataSource, columns} = props;
    const operationColumn = generateTableOperationColumn(this);
    const antDColumns = fieldDefsToAntdTableColumnMapper(columns);
    this.state = {
      dataSource,
      count: dataSource.length,
      columns: [
        operationColumn,
        ...antDColumns
      ],
      filteredInfo: null,
      sortedInfo: null,
    };
  }

  handleChange = (pagination, filters, sorter) => {
    // console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  clearFilters = () => {
    this.setState({filteredInfo: null});
  };

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null,
    });
  };

  handleDelete = key => {
    // @ts-ignore
    // console.log("called handle delete", key);
    const dataSource = [...this.state.dataSource];
    this.setState({dataSource: dataSource.filter(item => item._id !== key)});
  };

  render() {
    // @ts-ignore
    return (
      <div>
        <div className="table-operations">
          <Button type="primary">
            <Link href="/add">
              <a>AddLink</a>
            </Link>
          </Button>
        </div>
        <Table {...this.props} dataSource={this.state.dataSource} columns={this.state.columns}/>
      </div>
    );
  }
}

// @ts-ignore
export default DynamicTable;

function generateTableOperationColumn(tableInstance) {
  return {
    title: 'operation',
    dataIndex: 'operation',
    key: "operation",
    render: (text, record) =>
      tableInstance.state.dataSource.length >= 1 ? (
        <div>
          <Link href={{pathname: '/edit', query: {modelDefId: record._id}}}>
            <a>Edit</a>
          </Link>
          <Popconfirm title="Sure to delete?" onConfirm={() => tableInstance.handleDelete(record._id)}>
            <a>Delete</a>
          </Popconfirm>
        </div>
      ) : null,
  }
}
