import {modelDefsDataForAntDTable} from "../../gvc-meta-data-lib/src";

export function getSorterFunction(fieldDef) {
  const {name, type} = fieldDef;
  if (type === "string") {
    //TODO find logic of how to sort by actual name
    return ((a, b) => a[name].length - b[name].length)
  } else if (type === "int" || "float") {
    return ((a, b) => a[name] - b[name])
  }
}

export function addSorterFunction(fieldDef, antDColumn) {
  const {type} = fieldDef;
  if (type === "string" || type === "int" || type === "float") {
    antDColumn["sorter"] = getSorterFunction(fieldDef)
  }
}

//TODO How it will give uniq values if array values are object ?
export function getUniqueValues(values: Array<any>) {
  // @ts-ignore
  return [...new Set(values)];
}

export function getAllValues(array, name) {
  //TODO Add logic to remove or display blank values

  return array.map(item => {
    let arrayItem = item[name];
    // console.log("typeof ", typeof arrayItem);
    let type = typeof arrayItem;
    // TODO move this logic to main sanitization logic
    if (type === "string") {
      if (arrayItem === "true") return true;
      if (arrayItem === "false") return false;
    }
    return arrayItem;
  })
}

export function generateFilterColumnData(fieldDef, antDColumn) {
  const {name, type} = fieldDef;
  if (type === "boolean") {
    /*Note: Setting 'value' field to boolean true or false is not allowed and will throw
    error, so we used string 'true' and 'false' here */
    antDColumn["filters"] = [
      {
        text: 'True',
        value: 'true',
      },
      {
        text: 'False',
        value: 'false',
      }]
  } else {
    const allValues = getAllValues(modelDefsDataForAntDTable, name);
    let uniqueValues = getUniqueValues(allValues);
    // console.log("uniq values",uniqueValues);
    antDColumn["filters"] = uniqueValues.map((item) => ({text: item, value: item}));
  }
}

export function addFilterFunction(fieldDef, antDColumn) {
  const {name, type} = fieldDef;
  if (type === "string") {
    antDColumn["onFilter"] = (value, record) => record[name].indexOf(value) === 0;
  } else if (type === "int" || "float") {
    antDColumn["onFilter"] = (value, record) => record[name] === value;
  } else if (type === "boolean") {
    antDColumn["onFilter"] = (value, record) => {
      /*Note: Here 'value' contains either 'true' or 'false' defined in 'filters' config
       so we need to convert them to string for comparison */
      return record[name].toString() === value;
    }
  }

  generateFilterColumnData(fieldDef, antDColumn);

}

export function fieldDefsToAntdTableColumnMapper(fieldDefs) {
  return fieldDefs.map(fieldDef => {
    const {name, label} = fieldDef;
    let antDColumn = {
      title: label,
      dataIndex: name,
      key: name,
      defaultSortOrder: 'descend'
    };

    addSorterFunction(fieldDef, antDColumn);
    addFilterFunction(fieldDef, antDColumn);

    return antDColumn;
  })
}
