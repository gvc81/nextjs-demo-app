import React from 'react';
import Document, {Html, Head, Main, NextScript, DocumentContext} from 'next/document';


const appBackground = {
  fontFamily: "sans-serif",
  height: "100%"
};

export default class UIDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return {...initialProps};
  }

  render() {
    return (
      <Html>
        <Head/>
        <body style={appBackground}>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    );
  }
}
