import React from "react";
import {DynamicForm} from "../core-components";
export default function (props) {
  return <DynamicForm {...props} initialValues={null}/>
};
