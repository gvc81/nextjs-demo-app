import React from "react";
import {DynamicTable} from "../core-components";
import { modelDefsDataForAntDTable,modelDefFields } from "../gvc-meta-data-lib/src";
// import fixedModelDefsData from "../data/fixedModelDefs.json";
/** TODO Data sanitization is required as it is creating issues
 * with creating unique values.
 * Eg. some boolean values are string and some are actual boolean
 * businessKeys are not proper, some are string and some are array
 * **/
// Here props is {} object

export default function (props) {
  return (
    <div>
      <DynamicTable
        showHeader={true}
        bordered
        title={() => "Test table"}
        rowKey={"_id"}
        dataSource={modelDefsDataForAntDTable}
        columns={modelDefFields}
      />;
    </div>
  );
}
