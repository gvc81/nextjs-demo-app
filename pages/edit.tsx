import React from "react";
import {useRouter} from "next/router";
import {DynamicForm} from "../core-components";
import {modelDefsDataForAntDTable} from "../gvc-meta-data-lib/src";

export default function (props) {
  const router = useRouter();
  const {modelDefId} = router.query;
  const modelDef = modelDefsDataForAntDTable.find(modelDef => modelDef._id === modelDefId);
  return <DynamicForm {...props} initialValues={modelDef} isEdit={true} />
};
