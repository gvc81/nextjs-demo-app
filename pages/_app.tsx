import React from 'react';
import App from 'next/app';
import "./App.css";
import {withRouter} from 'next/router'

/*Note that here default keyword is required ny nextjs (need to find out why)*/
class UIApp extends App {
  render() {
    const {Component, pageProps} = this.props;
    return (
      <Component {...pageProps} />
    );
  }
}

export default withRouter(UIApp);
