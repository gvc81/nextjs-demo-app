import {getUniqueValues} from "../core-components/utils/DynamicTableUtils";
import {duplicateStringValueArray, uniqueStringArray} from "./fixtures/DynamicTableUtilsFixture";

describe("DynamicTableUtils",()=>{
  test('DynamicTableUtils: getUniqueValues from duplicate string value array', () => {
    const result = getUniqueValues(duplicateStringValueArray);
    expect(result.length === uniqueStringArray.length).toEqual(true);
  });
});
