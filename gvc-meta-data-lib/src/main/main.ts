import path from "path";
import {modelDefsData} from "../../data/modeldefs-data";
import {
  addMissingOwnerId,
  fixBooleanValues,
  MODELDEF_IS_BASE_MODEL,
  MODELDEF_OWNER_ID
} from "../utils/ModelDefDataUtils";
import {createJsonFile} from "../utils/JsonFileUtils";

const fixedModelDefs = addMissingOwnerId(modelDefsData,[MODELDEF_OWNER_ID]);
const dataDirPath = path.resolve(`${__dirname}/../../data`);
const FIXED_DATA_FILE_NAME = "fixed_model_defs.json";
const finalDataFilePath = `${dataDirPath}/${FIXED_DATA_FILE_NAME}`;

async function main() {
  await createJsonFile(fixedModelDefs, finalDataFilePath);
}

//Note: create fixedModelDefs.json file manually if it does'nt exist before executing this function
main();


