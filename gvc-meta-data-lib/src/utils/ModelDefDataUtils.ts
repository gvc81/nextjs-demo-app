import {
  booleanToStringBoolean,
  commaSeparatedStringToArray,
  createEmptyValue,
  isEmptyString,
  stringArrayToCommaSeparatedString
} from "./DataUtils";
export const businessKeysPropertyName = "businessKeys";

export const MODELDEF_SHOW_TABLE_OPERATION="showTableOperation";
export const MODELDEF_IS_BASE_MODEL="isBaseModel";
export const MODELDEF_OWNER_ID="ownerId";

export function fixBusinessKeysForAntDTableRepresentation(modelDefs) {
  return modelDefs.map(modelDef => {
    const {businessKeys, name} = modelDef;
    modelDef = createEmptyValue(modelDef,businessKeysPropertyName);
    try {
      modelDef = stringArrayToCommaSeparatedString(modelDef, businessKeysPropertyName)
    } catch (error) {
      console.error(`Failed to parse ${businessKeys} for modeldef ${name} while fixing business keys: `, businessKeys);
    }
    return modelDef;
  });
}

export function fixEmptyBusinessKeys(modelDef) {
  const {businessKeys}  = modelDef;
  if(!businessKeys || isEmptyString(businessKeys)){
    modelDef[businessKeysPropertyName] = [];
    return {...modelDef};
  }
  return {...modelDef};
}

export function fixStringBusinessKeys(modelDef) {
  return commaSeparatedStringToArray(modelDef,businessKeysPropertyName);
}

export function convertBusinessKeysToArray(modelDefs) {
  return modelDefs.map(modelDef=>{
    let fixedModelDef = fixEmptyBusinessKeys(modelDef);
    fixedModelDef = fixStringBusinessKeys(fixedModelDef);
    return  {...fixedModelDef};
  })
}

export function fixBooleanValues(modelDefs,propertyNames) {
  return modelDefs.map(modelDef=>{
    propertyNames.forEach(propertyName=>{
      let value = modelDef[propertyName];
      modelDef[propertyName] = booleanToStringBoolean(value);
    });
    return {...modelDef};
  });
}

export function addMissingOwnerId(modelDefs,propertyNames) {
  return modelDefs.map(modelDef=>{
     propertyNames.forEach(propertyName=>{
       modelDef[propertyName] = !modelDef[propertyName] ? "a@a.com" : modelDef[propertyName];
     });
     return {...modelDef};
  });
}
