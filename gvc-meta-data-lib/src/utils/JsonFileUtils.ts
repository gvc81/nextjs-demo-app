const fs = require("fs");
const writeJsonFile = require('write-json-file');
const READ_WRITE_MODE=0o666;

export function createJsonFile(jsonData, fileName) {
  return writeJsonFile(fileName, jsonData,{mode:READ_WRITE_MODE,indent:"  "});
}
