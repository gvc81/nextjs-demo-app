// Note that following functions mutate object var
import * as _ from "lodash";

export const NOT_AVAILABLE = "Not Available";

export function stringArrayToCommaSeparatedString(object, propertyName) {
  const objectPropertyValue = object[propertyName];
  if (Array.isArray(objectPropertyValue)) {
    object[propertyName] = objectPropertyValue.join(",");
    return object;
  }
  //TODO return clone ?
  return object;
}

export function isStringRepresentationOfArray(object, propertyName) {
  const objectPropertyValue = object[propertyName];
  return !isEmptyString(objectPropertyValue) && objectPropertyValue.includes("[");
}

export function isEmptyString(str) {
  return typeof str === "string" && str.trim().length === 0;
}

export function stringToArray(object, propertyName) {
  const objectPropertyValue = object[propertyName];
  return JSON.parse(objectPropertyValue);
}

export function stringToCommaSeparatedString(object, propertyName) {
  if (isStringRepresentationOfArray(object, propertyName)) {
    object[propertyName] = stringToArray(object, propertyName);
    return stringArrayToCommaSeparatedString(object, propertyName);
  }
  //TODO return clone ?
  return object;
}

export function isArrayEmpty(object, propertyName) {
  const propertyValue = object[propertyName];
  return _.isArray(propertyValue) && propertyValue.length === 0
}

export function createEmptyValue(object, propertyName) {
  const propertyValue = object[propertyName];
  if (!propertyValue || isArrayEmpty(object, propertyName) || isEmptyString(propertyValue)) {
    object[propertyName] = NOT_AVAILABLE;
  }
  return {...object};
}

export function commaSeparatedStringToArray(object, propertyName) {
  const propertyValue = object[propertyName];
  if (propertyValue && typeof propertyValue === "string") {
    // @ts-ignore
    object[propertyName] = propertyValue.split(",");
  }
  return {...object};
}

export function booleanToStringBoolean(value) {
  const type = typeof value;
  if (type === "boolean") {
    return value ? "true" : "false";
  } else {
    // console.log(`Warning: boolean to string boolean conversion is not possible as type is not boolean but found: ${type}`);
    return value;
  }
}
