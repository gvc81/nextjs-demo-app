import {fixBusinessKeysForAntDTableRepresentation} from "../src/utils/ModelDefDataUtils";

export const modelDefsData = [
  {
    "_id": "PqDywsjCT8PiMxtQF",
    "name": "modeldefs",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "uqwf38fGENJS8C8LP",
        "name": "name",
        "type": "string",
        "label": "Name",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "HzWPJstYue57abBo2",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "Zca7ZHj4TBgXGzqCR",
        "name": "showTableOperation",
        "type": "boolean",
        "label": "Show Table Operation",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "wxbgpJCrxuCHc9tN2",
        "name": "isBaseModel",
        "type": "boolean",
        "label": "Is Base Model",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "2Ji5W4NkERQMRGjpm",
        "name": "ownerId",
        "type": "combo",
        "model_name": "user",
        "display_value": "user_name",
        "option_value": "_id",
        "label": "Owner Id",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "hMKHMcP7nE6XNughl",
        "name": "businessKeys",
        "type": "string",
        "label": "Business Keys",
        "defaultValue": "",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "summaryProperty": ""
  },
  {
    "_id": "kFNS58txBQPzRzvup",
    "name": "fielddefs",
    "ownerId": "a@a.com",
    "isBaseModel": "true",
    "showTableOperation": "false",
    "businessKeys": [
      "name",
      "modelDefName"
    ],
    "fields": [
      {
        "_id": "HAyqnrMAf5PNt3Zyy",
        "name": "name",
        "type": "string",
        "label": "Field Name",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "y9NQnqwrmhDHFqEuG",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "NWPQ49nTuecTCH36u",
        "name": "label",
        "type": "string",
        "label": "Field Label",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "vsggsS7x3Sx26yqZG",
        "name": "type",
        "type": "string",
        "label": "Field Type",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "zfshNhAQjfaov5rKg",
        "name": "modelDefId",
        "type": "string",
        "label": "Model Def Id",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "d5kZDebzTieWGr4ra",
        "name": "modelDefName",
        "type": "string",
        "label": "Model Def Name",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "djEH7zHb46LeseD49",
        "name": "model_name",
        "type": "string",
        "label": "Model Name",
        "isShownInTable": "false",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false"
      },
      {
        "_id": "du9jvSYESfcFStb25",
        "name": "display_value",
        "type": "string",
        "label": "Display Value",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "xt74ppjWCeRFGiuSx",
        "name": "option_value",
        "type": "string",
        "label": "Option Value",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "8gtJhTZYRsWzFemqD",
        "name": "hide",
        "type": "boolean",
        "label": "Is Hidden",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "TtaWxkuxq73R3uLLr",
        "name": "defaultValue",
        "type": "string",
        "label": "Default Value",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "xMKHMcP7nE6XNuXom",
        "name": "isShownInTable",
        "type": "boolean",
        "label": "Is Shown In Table",
        "defaultValue": "true",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "hMKHMcP7nE6XNughk",
        "name": "isReadOnly",
        "type": "boolean",
        "label": "Is Read Only",
        "defaultValue": "false",
        "modelDefId": "kFNS58txBQPzRzvup",
        "modelDefName": "fielddefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "YdcBPGj4Hm4Bni6G8",
    "name": "user",
    "isBaseModel": "true",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "businessKeys": [
      "email"
    ],
    "fields": [
      {
        "_id": "92tHMANjtdovwwwRP",
        "name": "email",
        "label": "Email",
        "modelDefId": "YdcBPGj4Hm4Bni6G8",
        "modelDefName": "user",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "o9obCRqBF9SomcwjA",
        "name": "password",
        "type": "password",
        "hide": "true",
        "label": "Password",
        "modelDefId": "YdcBPGj4Hm4Bni6G8",
        "modelDefName": "user",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "fnZoa9LPaDxNxj3i3",
        "name": "roles",
        "type": "drop_down",
        "model_name": "role",
        "display_value": "name",
        "multiple": "true",
        "label": "Roles",
        "modelDefId": "YdcBPGj4Hm4Bni6G8",
        "modelDefName": "user",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "nX59h6CfaFfXhYCo4",
        "name": "meteorUserId",
        "type": "string",
        "label": "Meteor User Id",
        "modelDefId": "YdcBPGj4Hm4Bni6G8",
        "modelDefName": "user",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "FaTkxSHE4NxGCicxG",
        "name": "name",
        "label": "Name",
        "modelDefId": "YdcBPGj4Hm4Bni6G8",
        "modelDefName": "user",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "NEHhae59NZZ8L4QKg",
        "name": "_id",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "YdcBPGj4Hm4Bni6G8",
        "modelDefName": "user",
        "hide": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "5CRT6SFWs9tarogDi",
    "name": "users",
    "isBaseModel": "true",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "businessKeys": [
      "_id"
    ],
    "fields": [
      {
        "_id": "hKGzfjbo2kZGxqE62",
        "name": "_id",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "5CRT6SFWs9tarogDi",
        "modelDefName": "users",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "bGgAZJuw8kFE43ykA",
        "name": "emails",
        "label": "EMails",
        "modelDefId": "5CRT6SFWs9tarogDi",
        "modelDefName": "users",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "tcAwWQjLEX9zBqb3m",
        "name": "profile",
        "label": "Profile",
        "modelDefId": "5CRT6SFWs9tarogDi",
        "modelDefName": "users",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "Wb4fx6TfiX5dwr8Z3",
        "name": "roles",
        "label": "Roles",
        "modelDefId": "5CRT6SFWs9tarogDi",
        "modelDefName": "users",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "KYxDyyJ5ExXouGwZN",
    "name": "role",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "vePoTDSh4scbrba2g",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "KYxDyyJ5ExXouGwZN",
        "modelDefName": "role",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "rQCSePpGektauyN7R",
        "name": "name",
        "label": "Name",
        "modelDefId": "KYxDyyJ5ExXouGwZN",
        "modelDefName": "role",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "PvcCG9FRzL9Lb6shv",
        "name": "read",
        "type": "boolean",
        "label": "Can Read",
        "modelDefId": "KYxDyyJ5ExXouGwZN",
        "modelDefName": "role",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "eAHpXz6tDSNeMJ8Mx",
        "name": "insert",
        "type": "boolean",
        "label": "Can Insert",
        "modelDefId": "KYxDyyJ5ExXouGwZN",
        "modelDefName": "role",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "gjqaYPNYrpYFN5CW4",
        "name": "update",
        "type": "boolean",
        "label": "Can Update",
        "modelDefId": "KYxDyyJ5ExXouGwZN",
        "modelDefName": "role",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "6GZxzfwks8GpffuHH",
        "name": "delete",
        "type": "boolean",
        "label": "Can Delete",
        "modelDefId": "KYxDyyJ5ExXouGwZN",
        "modelDefName": "role",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "QJwzceddLFAHgcPaE",
    "name": "group",
    "isBaseModel": "true",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "ZPAZfybeT3CCvfAjA",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "QJwzceddLFAHgcPaE",
        "modelDefName": "group",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "6zKhxWka589y4dX3B",
        "name": "name",
        "label": "Name",
        "modelDefId": "QJwzceddLFAHgcPaE",
        "modelDefName": "group",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "ED2ouT2EeYHWTqbaP",
    "name": "appfeatures",
    "isBaseModel": "false",
    "ownerId": "a@a.com",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "2fYbvLf26fGTbYNuZ",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "ED2ouT2EeYHWTqbaP",
        "modelDefName": "appfeatures",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "SFyPtzZybrp4kzR4f",
        "name": "user",
        "type": "combo",
        "model_name": "user",
        "display_value": "email",
        "multiple": "false",
        "label": "User",
        "modelDefId": "ED2ouT2EeYHWTqbaP",
        "modelDefName": "appfeatures",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "yoiC3Wq8PdBvN7H9f",
        "name": "enabled",
        "type": "boolean",
        "label": "Is Enabled",
        "modelDefId": "ED2ouT2EeYHWTqbaP",
        "modelDefName": "appfeatures",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "YvY8pviXQmfLZcTNR",
        "name": "name",
        "type": "combo",
        "model_name": "app_feature_names",
        "display_value": "name",
        "multiple": "false",
        "label": "Name",
        "modelDefId": "ED2ouT2EeYHWTqbaP",
        "modelDefName": "appfeatures",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "showTableOperation": "false",
    "summaryProperty": ""
  },
  {
    "_id": "iAQPLTu9KdjTbgPqS",
    "name": "type",
    "isBaseModel": "true",
    "ownerId": "a@a.com",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "NFbCJnmZ69LRF2gWQ",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "iAQPLTu9KdjTbgPqS",
        "modelDefName": "type",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "vkXfzxQCSaZYaQQNa",
        "name": "name",
        "type": "string",
        "label": "Name",
        "modelDefId": "iAQPLTu9KdjTbgPqS",
        "modelDefName": "type",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "showTableOperation": "false"
  },
  {
    "_id": "zrttugdxn6rgqbtJb",
    "name": "app_feature_names",
    "isBaseModel": "false",
    "ownerId": "a@a.com",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "2afLnB5ezBZ6qQRda",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "zrttugdxn6rgqbtJb",
        "modelDefName": "app_feature_names",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "QqdnwQjd9Aj8KPLuE",
        "name": "name",
        "type": "string",
        "label": "Name",
        "modelDefId": "zrttugdxn6rgqbtJb",
        "modelDefName": "app_feature_names",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "showTableOperation": "false",
    "summaryProperty": ""
  },
  {
    "_id": "eeRrbjtTRe9vvJfc9",
    "name": "logdb",
    "isBaseModel": "false",
    "ownerId": "a@a.com",
    "businessKeys": [
      "_id"
    ],
    "fields": [
      {
        "_id": "zmbogkma6TAyTLNvD",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "eeRrbjtTRe9vvJfc9",
        "modelDefName": "logdb",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "WtpT6HkYqqRzEYxJK",
        "name": "source",
        "type": "string",
        "label": "Source",
        "modelDefId": "eeRrbjtTRe9vvJfc9",
        "modelDefName": "logdb",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "AaW7Ae4hF5ftRLtiy",
        "name": "mode",
        "type": "string",
        "label": "Mode",
        "modelDefId": "eeRrbjtTRe9vvJfc9",
        "modelDefName": "logdb",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "cGwpXYsQWoWPnhJyN",
        "name": "description",
        "type": "string",
        "label": "Description",
        "modelDefId": "eeRrbjtTRe9vvJfc9",
        "modelDefName": "logdb",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "showTableOperation": "false",
    "summaryProperty": ""
  },
  {
    "_id": "aeRrbjtTRe9vvJfc6",
    "name": "migrations",
    "isReadOnly": "true",
    "isBaseModel": "true",
    "ownerId": "a@a.com",
    "businessKeys": [
      "version",
      "lockedAt"
    ],
    "fields": [
      {
        "_id": "ambogkma6TAyTLNvD",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "aeRrbjtTRe9vvJfc6",
        "modelDefName": "migrations",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "btpT6HkYqqRzEYxJK",
        "name": "version",
        "type": "string",
        "label": "Version",
        "modelDefId": "aeRrbjtTRe9vvJfc6",
        "modelDefName": "migrations",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "caW7Ae4hF5ftRLtiy",
        "name": "locked",
        "type": "boolean",
        "label": "Is Locked",
        "modelDefId": "aeRrbjtTRe9vvJfc6",
        "modelDefName": "migrations",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "dGwpXYsQWoWPnhJyN",
        "name": "lockedAt",
        "type": "date",
        "label": "Locked Date",
        "modelDefId": "aeRrbjtTRe9vvJfc6",
        "modelDefName": "migrations",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "showTableOperation": "false"
  },
  {
    "_id": "beRrbjtTRe9vvJfc7",
    "name": "maconfig",
    "isReadOnly": "false",
    "isBaseModel": "true",
    "ownerId": "a@a.com",
    "businessKeys": [
      "propertyName",
      "propertyType",
      "propertyValue"
    ],
    "fields": [
      {
        "_id": "bmbogkma6TAyTLNvE",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "beRrbjtTRe9vvJfc7",
        "modelDefName": "maconfig",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "cmbogkma6TAyTLNvE",
        "name": "propertyName",
        "type": "string",
        "label": "Property Name",
        "isReadOnly": "false",
        "modelDefId": "beRrbjtTRe9vvJfc7",
        "modelDefName": "maconfig",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "dmbogkma6TAyTLNvE",
        "name": "propertyValue",
        "type": "string",
        "label": "Property Value",
        "isReadOnly": "false",
        "modelDefId": "beRrbjtTRe9vvJfc7",
        "modelDefName": "maconfig",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "embogkma6TAyTLNvE",
        "name": "propertyType",
        "type": "string",
        "label": "Property Type",
        "isReadOnly": "false",
        "modelDefId": "beRrbjtTRe9vvJfc7",
        "modelDefName": "maconfig",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "showTableOperation": "false"
  },
  {
    "_id": "qA2XeLE8jzu5HLF48",
    "name": "medical_bill",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "false",
    "summaryProperty": "",
    "businessKeys": "Not Available",
    "fields": [
      {
        "_id": "fBv9cGnkWZA79QjZo",
        "name": "firmName",
        "type": "string",
        "label": "Firm Name",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "8id3SwZa29aiAxCvC",
        "name": "billDate",
        "type": "date",
        "label": "Bill Date",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "R7zyrohNXrxKxEvfC",
        "name": "billNumber",
        "type": "string",
        "label": "Bill Number",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "EyTc9nvC8JKd5rGhj",
        "name": "amount",
        "type": "string",
        "label": "Amount",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "erqbHkb4wwE7PMwwp",
        "name": "invoiceNumber",
        "type": "string",
        "label": "Invoice Number",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "AqiHtEC6MoPuvRf3Y",
        "name": "receiptNumber",
        "type": "string",
        "label": "Receipt Number",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "qxhWKyqpEGoLvb375",
        "name": "discountAmount",
        "type": "string",
        "label": "Discount Amount",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "7Jdp749rHTwtkZMDB",
        "name": "firmAddress",
        "type": "text",
        "label": "Firm Address",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "bTtz7BG2EpL2vna4X",
        "name": "dueDate",
        "type": "date",
        "label": "Due Date",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "a5NRo8phRf2pPkkL2",
        "name": "tax",
        "type": "string",
        "label": "Tax",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "XaoEj44cu6Hs5GdvH",
        "name": "taxable",
        "type": "string",
        "label": "Taxable",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "4FAvLgYoK98ibx6AD",
        "name": "vat",
        "type": "string",
        "label": "VAT",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "GgS7ZHev6xjiTmzXy",
        "name": "tinNumber",
        "type": "string",
        "label": "Tin Number",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "6mbBzmF29uvx6Hg9i",
        "name": "vatParcent",
        "type": "string",
        "label": "VAT %",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "eXC2McTKrvuRaqWAJ",
        "name": "lessAmount",
        "type": "string",
        "label": "Less Amount",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "3hyCQw7YCv6AfuGxD",
        "name": "addAmount",
        "type": "string",
        "label": "Add Amount",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "eDGkMELeExCh5zrcu",
        "name": "roundedAmount",
        "type": "string",
        "label": "Rounded Amount",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "wDePYX6dZpn9NuoG4",
        "name": "toFirm",
        "type": "string",
        "label": "To Firm",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "ZDTCYximf2L6Bd2oM",
        "name": "toFirmAddress",
        "type": "text",
        "label": "To Firm Address",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "7Wbqm7iAnWJokvu3D",
        "name": "toFirmTinNumber",
        "type": "string",
        "label": "To Firm Tin Number",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "sD78NR4gqsTTR2AY6",
        "name": "phoneShop",
        "type": "string",
        "label": "Phone Shop",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "hkDnTJdANgDmniiPb",
        "name": "phoneResident",
        "type": "string",
        "label": "Phone Resident",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "Xt4QFsDbcTBvoogxA",
        "name": "subTitle",
        "type": "string",
        "label": "Sub Title",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "FgRDwH44DtMm3FosL",
        "name": "vatTin",
        "type": "string",
        "label": "Vat Tin",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "3Nxr7xCsfmy6nLoKq",
        "name": "cstTin",
        "type": "string",
        "label": "Cst Tin",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "FLgEWjPnAm9vnyE2D",
        "name": "cwef",
        "type": "string",
        "label": "C w.e.f.",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "S8b8jebYE5WTQihAQ",
        "name": "patientFirstName",
        "type": "string",
        "label": "Patient First Name",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "RuHsopCXQhx7tCPfv",
        "name": "patientLastName",
        "type": "string",
        "label": "Patient Last Name",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "mjfPHzAGf2waf4Yx8",
        "name": "patientAddress",
        "type": "string",
        "label": "Patient Address",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "rgafWWfuE4zXuZNfT",
        "name": "doctorFirstName",
        "type": "string",
        "label": "Doctor First Name",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "hR5wgQww4RfvxgCia",
        "name": "doctorLastName",
        "type": "string",
        "label": "Doctor Last Name",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "nbLLi8SMtkLKoTh8z",
        "name": "remarks",
        "type": "string",
        "label": "Remarks",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "gE4ZFr4rpadCFCbR3",
        "name": "drugLicenseNumber",
        "type": "string",
        "label": "Drug License Number",
        "hide": "false",
        "modelDefId": "qA2XeLE8jzu5HLF48",
        "modelDefName": "medical_bill",
        "isReadOnly": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "GutXizv5R36z82NiS",
    "name": "customerBill",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "false",
    "summaryProperty": "",
    "businessKeys": "Not Available",
    "fields": [
      {
        "_id": "ysRXm9azYFyTCGrBP",
        "name": "patientName",
        "type": "combo",
        "label": "Patient Name",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "fullName",
        "multiple": "false",
        "modelDefId": "GutXizv5R36z82NiS",
        "modelDefName": "customerBill",
        "isShownInTable": "false"
      },
      {
        "_id": "wWHmSkcF5pPQDsGNi",
        "name": "productDetails",
        "type": "datatable",
        "label": "Product Details",
        "hide": "false",
        "isReadOnly": "false",
        "dataTableModelName": "productOrder",
        "dataTableForeignKeyName": "customerBillId",
        "modelDefId": "GutXizv5R36z82NiS",
        "modelDefName": "customerBill",
        "isShownInTable": "false"
      },
      {
        "_id": "eyW67xcReRhjdv2Mj",
        "name": "defaultBillFormat",
        "type": "combo",
        "label": "Default Bill Format",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "medical_bill",
        "display_value": "firmName",
        "option_value": "firmName",
        "multiple": "false",
        "modelDefId": "GutXizv5R36z82NiS",
        "modelDefName": "customerBill",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "QX4zgsumtnrwTSEqq",
    "name": "test",
    "ownerId": "a@a.com",
    "showTableOperation": "true",
    "isBaseModel": "true",
    "businessKeys": "Not Available",
    "summaryProperty": "amount",
    "fields": [
      {
        "_id": "pNABSw8dreYq75Jcw",
        "name": "date",
        "type": "date",
        "label": "Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QX4zgsumtnrwTSEqq",
        "modelDefName": "test",
        "isShownInTable": "false"
      },
      {
        "_id": "7sFqo6dZusKE2ympv",
        "name": "bool",
        "type": "boolean",
        "label": "Boolean",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QX4zgsumtnrwTSEqq",
        "modelDefName": "test",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "MPH9wH7swXo9guZwA",
    "name": "implinks",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "url",
      "desc",
      "category"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "6PkGXTrH9JpcCdo7J",
        "name": "url",
        "type": "string",
        "label": "Url",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "MPH9wH7swXo9guZwA",
        "modelDefName": "implinks",
        "isShownInTable": "false"
      },
      {
        "_id": "MQf4ZNvgApxhd9Qit",
        "name": "desc",
        "type": "string",
        "label": "Description",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "MPH9wH7swXo9guZwA",
        "modelDefName": "implinks",
        "isShownInTable": "false"
      },
      {
        "_id": "wWSGAufJiojXyWc8J",
        "name": "category",
        "type": "combo",
        "label": "Category",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "catagory",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "MPH9wH7swXo9guZwA",
        "modelDefName": "implinks",
        "isShownInTable": "false"
      },
      {
        "_id": "aQDGaZdp93Sa88Q85",
        "name": "addedOn",
        "type": "date",
        "label": "Added On",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "MPH9wH7swXo9guZwA",
        "modelDefName": "implinks"
      }
    ]
  },
  {
    "_id": "CBqxu5fu7vitNF9JR",
    "name": "info",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "date",
      "desc",
      "category"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "z7jAHHpv7Mj3yYnaB",
        "name": "date",
        "type": "date",
        "label": "Date Added",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "CBqxu5fu7vitNF9JR",
        "modelDefName": "info"
      },
      {
        "_id": "ceD5uRdqJaQkRpxsf",
        "name": "desc",
        "type": "text",
        "label": "Description",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "CBqxu5fu7vitNF9JR",
        "modelDefName": "info"
      },
      {
        "_id": "vrySbZzWQwdRQRYgN",
        "name": "category",
        "type": "combo",
        "label": "Category",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "catagory",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "CBqxu5fu7vitNF9JR",
        "modelDefName": "info"
      },
      {
        "_id": "bsSxpxQxkym5cdF9n",
        "name": "importance",
        "type": "string",
        "label": "Importance",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "CBqxu5fu7vitNF9JR",
        "modelDefName": "info"
      }
    ]
  },
  {
    "_id": "3sSEMchKBnDXFANcN",
    "name": "dev_tips",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "description"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "XLZKHKphH7dzDwqL7",
        "name": "description",
        "type": "string",
        "label": "Description",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "gssQvA37cshZwRyQu",
        "name": "date",
        "type": "date",
        "label": "Date",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "RDazvxTSESzMKPdHk",
        "name": "os",
        "type": "string",
        "label": "Os",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "RGDJmqayqh7QENJss",
        "name": "architecture",
        "type": "string",
        "label": "Architecture",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "zFwhWRTraKQKGrM32",
        "name": "platform",
        "type": "string",
        "label": "Platform",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "rhtC2EwiHpoeYnop7",
        "name": "application_name",
        "type": "string",
        "label": "Application Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "oWKs6CmSAFDuj8Hxj",
        "name": "type",
        "type": "string",
        "label": "Type",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      },
      {
        "_id": "nBCHwm7teeyfxKGS7",
        "name": "framework",
        "type": "combo",
        "label": "Framework",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips",
        "model_name": "framework",
        "display_value": "name",
        "option_value": "",
        "multiple": false
      },
      {
        "_id": "TPy7XDdDSS5L6KkKy",
        "name": "language",
        "type": "combo",
        "label": "Language",
        "defaultValue": "Javascript",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips",
        "model_name": "programming_languages",
        "display_value": "name",
        "option_value": "",
        "multiple": false
      },
      {
        "_id": "vvodwH8ZRJHJPR8Dy",
        "name": "importance",
        "type": "string",
        "label": "Importance",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "3sSEMchKBnDXFANcN",
        "modelDefName": "dev_tips"
      }
    ]
  },
  {
    "_id": "PjEJxziCW927RuiCY",
    "name": "item",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name",
      "category"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "YBwaMJSBLCjZmAsiS",
        "name": "name",
        "type": "string",
        "label": "Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "PjEJxziCW927RuiCY",
        "modelDefName": "item"
      },
      {
        "_id": "iuXB7r8vmgc5J7zw4",
        "name": "category",
        "type": "combo",
        "label": "Category",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "catagory",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "PjEJxziCW927RuiCY",
        "modelDefName": "item"
      }
    ]
  },
  {
    "_id": "GMStXh39Wixj5upwK",
    "name": "collectiveexp",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "desc"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "A7gCKB7uMBmgxXoNR",
        "name": "desc",
        "type": "text",
        "label": "Description",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "GMStXh39Wixj5upwK",
        "modelDefName": "collectiveexp"
      },
      {
        "_id": "MzjrWgT72s9wXyTvQ",
        "name": "dateAdded",
        "type": "date",
        "label": "Date Added",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "GMStXh39Wixj5upwK",
        "modelDefName": "collectiveexp"
      },
      {
        "_id": "ZBHaMEK5axFcpXS8R",
        "name": "importance",
        "type": "string",
        "label": "Importance",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "GMStXh39Wixj5upwK",
        "modelDefName": "collectiveexp"
      }
    ]
  },
  {
    "_id": "g79LBuGHqrp4YEunA",
    "name": "framework",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "SiBh7xxRvscGnG7e4",
        "name": "name",
        "type": "string",
        "label": "Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "g79LBuGHqrp4YEunA",
        "modelDefName": "framework"
      }
    ]
  },
  {
    "_id": "MXmbSuCG63EXwQ2dM",
    "name": "programming_languages",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "4CPgjDsdoGgjF2hur",
        "name": "name",
        "type": "string",
        "label": "Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "MXmbSuCG63EXwQ2dM",
        "modelDefName": "programming_languages"
      }
    ]
  },
  {
    "_id": "ZFAmv6CTK6nNAYwsd",
    "name": "catagory",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "summaryProperty": "",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "beGg3AActXosA9p9j",
        "name": "name",
        "type": "string",
        "label": "Name",
        "hide": "false",
        "modelDefId": "ZFAmv6CTK6nNAYwsd",
        "modelDefName": "catagory",
        "isReadOnly": "false",
        "defaultValue": "",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "ZFdJ22aEAX84EHLNq",
    "name": "monthly_expenditure",
    "ownerId": "a@a.com",
    "showTableOperation": "true",
    "isBaseModel": "true",
    "summaryProperty": "amount",
    "businessKeys": [
      "date",
      "desc",
      "amount",
      "catagory"
    ],
    "fields": [
      {
        "_id": "cgm3fGbjxKNPrzMZB",
        "name": "date",
        "type": "date",
        "label": "Date",
        "hide": "false",
        "modelDefId": "ZFdJ22aEAX84EHLNq",
        "modelDefName": "monthly_expenditure",
        "isReadOnly": "false",
        "defaultValue": "",
        "isShownInTable": "false"
      },
      {
        "_id": "bzuBK6m526hijL6wQ",
        "name": "desc",
        "type": "string",
        "label": "Desc",
        "hide": "false",
        "modelDefId": "ZFdJ22aEAX84EHLNq",
        "modelDefName": "monthly_expenditure",
        "isReadOnly": "false",
        "defaultValue": "",
        "isShownInTable": "false"
      },
      {
        "_id": "R8z2pK2XqEZALxmAE",
        "name": "catagory",
        "type": "combo",
        "label": "Catagory",
        "hide": "false",
        "model_name": "catagory",
        "display_value": "name",
        "option_value": "",
        "multiple": "false",
        "modelDefId": "ZFdJ22aEAX84EHLNq",
        "modelDefName": "monthly_expenditure",
        "isReadOnly": "false",
        "defaultValue": "",
        "isShownInTable": "false"
      },
      {
        "_id": "hp6sLCQAL9idujPNB",
        "name": "amount",
        "type": "int",
        "label": "Amount",
        "hide": "false",
        "modelDefId": "ZFdJ22aEAX84EHLNq",
        "modelDefName": "monthly_expenditure",
        "isReadOnly": "false",
        "defaultValue": "",
        "isShownInTable": "false"
      },
      {
        "_id": "JJz4HLkEpvHbvoxZv",
        "name": "time",
        "type": "string",
        "label": "Time",
        "hide": "false",
        "modelDefId": "ZFdJ22aEAX84EHLNq",
        "modelDefName": "monthly_expenditure",
        "isReadOnly": "false",
        "defaultValue": "",
        "isShownInTable": "false"
      },
      {
        "_id": "FG65ccXpnquioSYba",
        "name": "forUser",
        "type": "combo",
        "label": "For User",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "",
        "multiple": "false",
        "modelDefId": "ZFdJ22aEAX84EHLNq",
        "modelDefName": "monthly_expenditure",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "LoneoQDZSaQAPDyxC",
    "name": "contacts",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "summaryProperty": "",
    "businessKeys": [
      "fullName"
    ],
    "fields": [
      {
        "_id": "jTwTRhTRLcaooWqSn",
        "name": "firstName",
        "type": "string",
        "label": "First Name",
        "hide": "false",
        "modelDefId": "LoneoQDZSaQAPDyxC",
        "modelDefName": "contacts",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "Prfy5DnvZN36QFrei",
        "name": "lastName",
        "type": "string",
        "label": "Last Name",
        "hide": "false",
        "modelDefId": "LoneoQDZSaQAPDyxC",
        "modelDefName": "contacts",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "eu4BgjizC9rrffmk4",
        "name": "fullName",
        "type": "string",
        "label": "Full Name",
        "hide": "false",
        "modelDefId": "LoneoQDZSaQAPDyxC",
        "modelDefName": "contacts",
        "isReadOnly": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "KTt7NRYMpBoiqHa6h",
    "name": "product",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "false",
    "summaryProperty": "",
    "fields": [
      {
        "_id": "f42hGMYFTLsKFyvhZ",
        "name": "name",
        "type": "string",
        "label": "Name",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "KTt7NRYMpBoiqHa6h",
        "modelDefName": "product",
        "isShownInTable": "false"
      },
      {
        "_id": "YnLrLvS7mKJA6JSxe",
        "name": "price",
        "type": "string",
        "label": "Price",
        "defaultValue": "60",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "KTt7NRYMpBoiqHa6h",
        "modelDefName": "product",
        "isShownInTable": "false"
      },
      {
        "_id": "a56qCNWDPHQpyBecX",
        "name": "date",
        "type": "date",
        "label": "Expiry Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "KTt7NRYMpBoiqHa6h",
        "modelDefName": "product",
        "isShownInTable": "false"
      }
    ],
    "businessKeys": [
      "name"
    ]
  },
  {
    "_id": "LRs7Xi2mruMkCHurb",
    "name": "mbill",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "false",
    "summaryProperty": "",
    "fields": [
      {
        "_id": "oZsHLJY2aHHLhchyR",
        "name": "firmName",
        "type": "string",
        "label": "Firm Name",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "LRs7Xi2mruMkCHurb",
        "modelDefName": "mbill",
        "isShownInTable": "false"
      },
      {
        "_id": "zNXu4NehusTogCsq4",
        "name": "productOrders",
        "type": "datatable",
        "label": "Product Details",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "productOrder",
        "modelDefId": "LRs7Xi2mruMkCHurb",
        "modelDefName": "mbill",
        "dataTableModelName": "productOrder",
        "dataTableForeignKeyName": "mbillId",
        "isShownInTable": "false"
      }
    ],
    "businessKeys": [
      "firmName"
    ]
  },
  {
    "_id": "Sq6Z2PMYgWRbw4A9r",
    "name": "productOrder",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "summaryProperty": "",
    "fields": [
      {
        "_id": "kBmRvQpPcZp2Eimoq",
        "name": "productName",
        "type": "combo",
        "label": "Product Name",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "medical_product",
        "display_value": "name",
        "option_value": "name",
        "multiple": "false",
        "modelDefId": "Sq6Z2PMYgWRbw4A9r",
        "modelDefName": "productOrder",
        "isShownInTable": "false"
      },
      {
        "_id": "DewForG98qdjfpGXb",
        "name": "qty",
        "type": "string",
        "label": "Qty",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "Sq6Z2PMYgWRbw4A9r",
        "modelDefName": "productOrder",
        "isShownInTable": "false"
      },
      {
        "_id": "Fbjekevu5jaoo76YQ",
        "name": "customerBillId",
        "type": "string",
        "label": "Customer Bill Id",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "Sq6Z2PMYgWRbw4A9r",
        "modelDefName": "productOrder",
        "isShownInTable": "false"
      }
    ],
    "businessKeys": [
      "productName",
      "qty",
      "customerBillId"
    ]
  },
  {
    "_id": "yZ9iz7veskk2aasNd",
    "name": "medical_product",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "false",
    "summaryProperty": "",
    "businessKeys": [
      "name",
      "company"
    ],
    "fields": [
      {
        "_id": "56edtn3GhjadFQbZp",
        "name": "name",
        "type": "string",
        "label": "Product Name",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "KBbcKtetnjuWQbWfG",
        "name": "company",
        "type": "string",
        "label": "Company",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "a9XritPnaL7dkNd8d",
        "name": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "upcw6sWybTGeNudDH",
        "name": "amount",
        "type": "string",
        "label": "Amount",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "ks2e28eTMgerZjTnw",
        "name": "manufacturingDate",
        "type": "date",
        "label": "Mfg",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "NPofYypeSqBXagSyW",
        "name": "batchNumber",
        "type": "string",
        "label": "Batch Number",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "XPKzCpA8hKmydfhwq",
        "name": "mrp",
        "type": "string",
        "label": "MRP",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "A8maWgAshDHqW65pm",
        "name": "rate",
        "type": "string",
        "label": "Rate",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "haWewKnv494ei6pcQ",
        "name": "tax",
        "type": "string",
        "label": "TAX%",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "GBdCtBZen8c4rPCFX",
        "name": "discount",
        "type": "string",
        "label": "Discount%",
        "hide": "false",
        "modelDefId": "yZ9iz7veskk2aasNd",
        "modelDefName": "medical_product",
        "isReadOnly": "false",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "LSZpN5iftDe42Dz2v",
    "name": "customer",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "summaryProperty": "",
    "fields": [
      {
        "_id": "kkmvhGMYBDMF9a5GY",
        "name": "fullName",
        "type": "string",
        "label": "Full Name",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "LSZpN5iftDe42Dz2v",
        "modelDefName": "customer",
        "isShownInTable": "false"
      }
    ],
    "businessKeys": [
      "fullName"
    ]
  },
  {
    "_id": "dpzJ3973pxzriXCmc",
    "name": "inputoutput",
    "ownerId": "a@a.com",
    "showTableOperation": "true",
    "isBaseModel": "true",
    "businessKeys": [
      "date",
      "inorout",
      "source",
      "bank",
      "amount"
    ],
    "summaryProperty": "amount",
    "fields": [
      {
        "_id": "Fz7fiA9zrA9DfXs2R",
        "name": "date",
        "type": "date",
        "label": "Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "YqehXH4HgSStoStQC",
        "name": "inorout",
        "type": "string",
        "label": "In Or Out",
        "defaultValue": "In",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "xtucMCS2vFwhGgYcr",
        "name": "source",
        "type": "string",
        "label": "Source",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "rB9EervetjMzx77MX",
        "name": "amount",
        "type": "int",
        "label": "Amount",
        "defaultValue": "0",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "G4fr25HByrp7jXWHZ",
        "name": "note",
        "type": "string",
        "label": "Note",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "B5XwFkGyZAhSTw6ts",
        "name": "owner",
        "type": "combo",
        "label": "Owner",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "",
        "multiple": "false",
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "8t4vDCbN6cBh8JB54",
        "name": "bank",
        "type": "combo",
        "label": "Bank",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "bank",
        "display_value": "name",
        "option_value": "",
        "multiple": "false",
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      },
      {
        "_id": "s4jTeAp9dwHxuDvv6",
        "name": "receivedDate",
        "type": "date",
        "label": "Received On",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput"
      },
      {
        "_id": "cjHmX9m3xKA2Z5s62",
        "name": "isReceived",
        "type": "boolean",
        "label": "Is Received",
        "defaultValue": "true",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "dpzJ3973pxzriXCmc",
        "modelDefName": "inputoutput",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "zFBwCjxttizGKaDFC",
    "name": "bank",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "GZ9ziyyXL74fXod7i",
        "name": "name",
        "type": "string",
        "label": "Name",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "zFBwCjxttizGKaDFC",
        "modelDefName": "bank",
        "isShownInTable": "false"
      },
      {
        "_id": "NZzZxqJHvWofMFvWe",
        "name": "address",
        "type": "string",
        "label": "Address",
        "defaultValue": "Parli",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "zFBwCjxttizGKaDFC",
        "modelDefName": "bank",
        "isShownInTable": "false"
      },
      {
        "_id": "2DQ45EXbgakZLSonJ",
        "name": "ifsCode",
        "type": "string",
        "label": "IFS Code",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "zFBwCjxttizGKaDFC",
        "modelDefName": "bank"
      }
    ]
  },
  {
    "_id": "QzY2fiGwgzb2uYnDt",
    "name": "issuetracker",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "desc"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "JeyE5emvgdwPXfa2i",
        "name": "desc",
        "type": "text",
        "label": "Description",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "dA2eKJm7ox88gTi7d",
        "name": "date",
        "type": "date",
        "label": "Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "nHK9oifdJSmdNgMC2",
        "name": "version",
        "type": "string",
        "label": "Version",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "3oChZN9g7vinRrKGF",
        "name": "projectName",
        "type": "combo",
        "label": "Project Name",
        "defaultValue": "MA av11",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "model_name": "project",
        "display_value": "name",
        "option_value": "",
        "multiple": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "MqPLgQ8NFxXWZw6ZP",
        "name": "malibVersion",
        "type": "string",
        "label": "MA Lib Version",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "qsgKNWqs5ia62PRRe",
        "name": "branchName",
        "type": "string",
        "label": "Branch Name",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "vA754Ymr78JXwXKSQ",
        "name": "resolvedInVersion",
        "type": "string",
        "label": "Resolved In Version",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "8kKnegWnjCw6tdW2a",
        "name": "isResolved",
        "type": "boolean",
        "label": "Is Resolved",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker",
        "isShownInTable": "false"
      },
      {
        "_id": "ogD8zE3AeBxRRZAQW",
        "name": "type",
        "type": "string",
        "label": "Issue Type",
        "defaultValue": "Issue",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker"
      },
      {
        "_id": "q6gLTxByvD3q25kYw",
        "name": "priority",
        "type": "string",
        "label": "Issue Priority",
        "defaultValue": "L",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "QzY2fiGwgzb2uYnDt",
        "modelDefName": "issuetracker"
      }
    ]
  },
  {
    "_id": "vHjsnRFxqxevoizLf",
    "name": "project",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "SojrgdpCLeKbyu3hg",
        "name": "name",
        "type": "string",
        "label": "Product Name",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "vHjsnRFxqxevoizLf",
        "modelDefName": "project",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "oJaqtSyfqzp3EDwd5",
    "name": "usecases",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "project",
      "desc"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "ma3t6yGuzZ26sRx3X",
        "name": "desc",
        "type": "text",
        "label": "Description",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "oJaqtSyfqzp3EDwd5",
        "modelDefName": "usecases",
        "isShownInTable": "false"
      },
      {
        "_id": "pQqX7tJ7QzovbYDkh",
        "name": "date",
        "type": "date",
        "label": "Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "oJaqtSyfqzp3EDwd5",
        "modelDefName": "usecases",
        "isShownInTable": "false"
      },
      {
        "_id": "6D3KPCkJm2o4Xz7Sb",
        "name": "project",
        "type": "combo",
        "label": "Project Name",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "project",
        "display_value": "name",
        "option_value": "",
        "multiple": "false",
        "modelDefId": "oJaqtSyfqzp3EDwd5",
        "modelDefName": "usecases",
        "isShownInTable": "false"
      },
      {
        "_id": "BYrbyyhFpGgFHpWdJ",
        "name": "expectedResult",
        "type": "text",
        "label": "Expected Result",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "oJaqtSyfqzp3EDwd5",
        "modelDefName": "usecases",
        "isShownInTable": "false"
      },
      {
        "_id": "XhGJK9e7K8R2vibng",
        "name": "currentResult",
        "type": "string",
        "label": "Current Result",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "oJaqtSyfqzp3EDwd5",
        "modelDefName": "usecases",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "Y5a8RSt4cd7XsW8dy",
    "name": "todo",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "date",
      "desc",
      "isDone",
      "priority"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "WwEo8THtJkCcbZ9Rs",
        "name": "date",
        "type": "date",
        "label": "Date added",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "Y5a8RSt4cd7XsW8dy",
        "modelDefName": "todo",
        "isShownInTable": "false"
      },
      {
        "_id": "5cZsbnq6YjtnQvk5o",
        "name": "desc",
        "type": "string",
        "label": "Description",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "Y5a8RSt4cd7XsW8dy",
        "modelDefName": "todo",
        "isShownInTable": "false"
      },
      {
        "_id": "u5WHwH8R7sr6ab8ZM",
        "name": "isDone",
        "type": "boolean",
        "label": "Is Done",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "Y5a8RSt4cd7XsW8dy",
        "modelDefName": "todo",
        "isShownInTable": "false"
      },
      {
        "_id": "k2AEurux2QEGyRTYJ",
        "name": "priority",
        "type": "string",
        "label": "Priority",
        "defaultValue": "L",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "Y5a8RSt4cd7XsW8dy",
        "modelDefName": "todo",
        "isShownInTable": "false"
      },
      {
        "_id": "ZZtuLTqbPyocfCp3o",
        "name": "completedDate",
        "type": "date",
        "label": "Completed On",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "Y5a8RSt4cd7XsW8dy",
        "modelDefName": "todo"
      },
      {
        "_id": "EQn3XS7ZgEgbeskR8",
        "name": "category",
        "type": "combo",
        "label": "Category",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "catagory",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "Y5a8RSt4cd7XsW8dy",
        "modelDefName": "todo"
      }
    ]
  },
  {
    "_id": "NE3m62BPHBXX6xLvu",
    "name": "fdr",
    "ownerId": "a@a.com",
    "showTableOperation": "true",
    "isBaseModel": "true",
    "businessKeys": [
      "fullName",
      "amount",
      "interestRate",
      "renewedDate",
      "bankName",
      "nominee"
    ],
    "summaryProperty": "amount",
    "fields": [
      {
        "_id": "MgL6ofZQNyf4sZAFY",
        "name": "fullName",
        "type": "combo",
        "label": "Name",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "",
        "multiple": false,
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "xWtmGEnp7S6ggMZw9",
        "name": "amount",
        "type": "int",
        "label": "Amount",
        "defaultValue": "0",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "DZ492zHfDXFDZFPEf",
        "name": "interestRate",
        "type": "string",
        "label": "Interest Rate",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "qgHjNEG2dwynF3QA9",
        "name": "days",
        "type": "int",
        "label": "Days ",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "dqvt8ahXMcXE5S72D",
        "name": "months",
        "type": "int",
        "label": "Months",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "hcCCcyK3eSNH4csXp",
        "name": "years",
        "type": "int",
        "label": "Years",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "YoGiorNbWPnopDBWn",
        "name": "renewedDate",
        "type": "date",
        "label": "Renewed Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "GoQJ89cMdej24f4nF",
        "name": "bankName",
        "type": "combo",
        "label": "Bank Name",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "bank",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "rx8C7EYdHLDeLKpCK",
        "name": "nominee",
        "type": "combo",
        "label": "Nominee",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "",
        "multiple": false,
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "QQioajMj7CRPwXewD",
        "name": "isMonthly",
        "type": "boolean",
        "label": "Is Monthly",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "dy3t6We5CBvZXQ7mf",
        "name": "maturityDate",
        "type": "date",
        "label": "Maturity Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "vR9h3M7q6xhzf4LEq",
        "name": "accountNo",
        "type": "string",
        "label": "Account No.",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "kft32vrx6WbhrfHmf",
        "name": "monthlyRupees",
        "type": "int",
        "label": "Monthly Rupees",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "f7SwRgnNGewby6zP7",
        "name": "maturityAmount",
        "type": "int",
        "label": "Maturity Amount",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "4ASdJk8ffZYZLh9KT",
        "name": "depositedDate",
        "type": "date",
        "label": "Deposited Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "82XJDMNMKoiHZTnhE",
        "name": "receiptNumber",
        "type": "string",
        "label": "Receipt No.",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "Pjze9GgMrJcjyoGKq",
        "name": "withdrawnFrom",
        "type": "date",
        "label": "Withdrawn From",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "KqiAoLsKt7BZoaWN4",
        "name": "withdrawnTo",
        "type": "date",
        "label": "Withdrawn To Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "H8dtqqXGeQvSChBqf",
        "name": "notes",
        "type": "string",
        "label": "Notes",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "v5hLnFr45HaedwuEx",
        "name": "isMatured",
        "type": "boolean",
        "label": "Is Matured",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "nHpHkrTf7Q2JLAgvJ",
        "name": "lastWithdrawnAmount",
        "type": "int",
        "label": "Last Withdrawn Amount",
        "defaultValue": "0",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "abQ79sGyDPfm3ceXM",
        "name": "withdrawnOnDate",
        "type": "date",
        "label": "Withdrawn On Date",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      },
      {
        "_id": "sjpThshTRxveG7QXw",
        "name": "hasSavingAccount",
        "type": "boolean",
        "label": "Has Saving Account",
        "defaultValue": "",
        "hide": "false",
        "isReadOnly": "false",
        "modelDefId": "NE3m62BPHBXX6xLvu",
        "modelDefName": "fdr",
        "isShownInTable": "false"
      }
    ]
  },
  {
    "_id": "xJRWaEG7kRgzuWeLp",
    "name": "bankaccount",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": "Not Available",
    "summaryProperty": "",
    "fields": [
      {
        "_id": "rrrem6KNb7GCJZqeA",
        "name": "bank",
        "type": "combo",
        "label": "Bank Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "bank",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      },
      {
        "_id": "6E8ns8iJ6yjB5MfQ7",
        "name": "owner",
        "type": "combo",
        "label": "Owner Name",
        "defaultValue": "Govardhan Chaudhari",
        "hide": false,
        "isReadOnly": false,
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "",
        "multiple": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      },
      {
        "_id": "DE84BZDb28xA9mqMB",
        "name": "accountNumber",
        "type": "string",
        "label": "Account Number",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      },
      {
        "_id": "ZFZSiJ64cSW9wZNwP",
        "name": "currentBalance",
        "type": "string",
        "label": "Current Balance",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      },
      {
        "_id": "jhNJ9bEwBxdgpZrZ7",
        "name": "currentBalanceDate",
        "type": "date",
        "label": "Current Balance Date",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      },
      {
        "_id": "wP6BCxpidy5JbGgyY",
        "name": "note",
        "type": "string",
        "label": "Note",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      },
      {
        "_id": "xSfYrvWjQa55GW9X9",
        "name": "accountType",
        "type": "string",
        "label": "Account Type",
        "defaultValue": "Saving",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "xJRWaEG7kRgzuWeLp",
        "modelDefName": "bankaccount"
      }
    ]
  },
  {
    "_id": "EJZeh7oAqcgkcuiEq",
    "name": "lended",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "date",
      "name"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "p8YDCZAhxaaQJeCwk",
        "name": "dateLended",
        "type": "date",
        "label": "Date Lended",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "EJZeh7oAqcgkcuiEq",
        "modelDefName": "lended"
      },
      {
        "_id": "keTtjzhqssmLKj2zy",
        "name": "lendedTo",
        "type": "combo",
        "label": "Lended To",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "customer",
        "display_value": "fullName",
        "option_value": "",
        "multiple": false,
        "modelDefId": "EJZeh7oAqcgkcuiEq",
        "modelDefName": "lended"
      },
      {
        "_id": "8ghKdaiJ9F9zaxauw",
        "name": "item",
        "type": "combo",
        "label": "Item Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "item",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "EJZeh7oAqcgkcuiEq",
        "modelDefName": "lended"
      },
      {
        "_id": "4keYtuFB6tMb5sfJ4",
        "name": "isReceived",
        "type": "boolean",
        "label": "Is Received",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "EJZeh7oAqcgkcuiEq",
        "modelDefName": "lended"
      },
      {
        "_id": "oY5iSdwrLCmEQGWmt",
        "name": "receivedOn",
        "type": "date",
        "label": "Received On",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "EJZeh7oAqcgkcuiEq",
        "modelDefName": "lended"
      }
    ]
  },
  {
    "_id": "MWCZHWrHe2c9R3cjQ",
    "name": "currentcash",
    "ownerId": "a@a.com",
    "showTableOperation": "true",
    "isBaseModel": "true",
    "businessKeys": [
      "date",
      "amount",
      "source"
    ],
    "summaryProperty": "amount",
    "fields": [
      {
        "_id": "9CaJ2dgYacd3W7LzX",
        "name": "date",
        "type": "date",
        "label": "Date",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "MWCZHWrHe2c9R3cjQ",
        "modelDefName": "currentcash"
      },
      {
        "_id": "8mYY5MnFFHsELN5sS",
        "name": "amount",
        "type": "int",
        "label": "Amount",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "MWCZHWrHe2c9R3cjQ",
        "modelDefName": "currentcash"
      },
      {
        "_id": "BiFqqwjSpqN2P7Xub",
        "name": "source",
        "type": "string",
        "label": "Source",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "MWCZHWrHe2c9R3cjQ",
        "modelDefName": "currentcash"
      },
      {
        "_id": "PLYprSo7Pbu7EPEGP",
        "name": "note",
        "type": "string",
        "label": "Note",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "MWCZHWrHe2c9R3cjQ",
        "modelDefName": "currentcash"
      }
    ]
  },
  {
    "_id": "eBRnMS5JMoXZNX9aS",
    "name": "medicine",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name",
      "company"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "5Yivt3Lr6atGe8t9M",
        "name": "name",
        "type": "string",
        "label": "Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "eBRnMS5JMoXZNX9aS",
        "modelDefName": "medicine"
      },
      {
        "_id": "4kQdvLFvQzDMJGczA",
        "name": "company",
        "type": "combo",
        "label": "Company",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "eBRnMS5JMoXZNX9aS",
        "modelDefName": "medicine",
        "model_name": "company",
        "display_value": "name",
        "option_value": "",
        "multiple": false
      },
      {
        "_id": "H4CdTqzQbm7edXXw7",
        "name": "note",
        "type": "string",
        "label": "Note",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "eBRnMS5JMoXZNX9aS",
        "modelDefName": "medicine"
      },
      {
        "_id": "EANfgbBhv8WyEmNed",
        "name": "power",
        "type": "string",
        "label": "Power",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "eBRnMS5JMoXZNX9aS",
        "modelDefName": "medicine"
      },
      {
        "_id": "KC6pXq4FxoGdfCxQk",
        "name": "contents",
        "type": "string",
        "label": "Contents",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "eBRnMS5JMoXZNX9aS",
        "modelDefName": "medicine"
      },
      {
        "_id": "obuAwgCmCYQubmchq",
        "name": "type",
        "type": "string",
        "label": "Type",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "eBRnMS5JMoXZNX9aS",
        "modelDefName": "medicine"
      }
    ]
  },
  {
    "_id": "x5gfvTGY83Ha8HTx7",
    "name": "company",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "summaryProperty": "",
    "fields": [
      {
        "_id": "YFHFKFXpG88DaoyEm",
        "name": "name",
        "type": "string",
        "label": "Name",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "modelDefId": "x5gfvTGY83Ha8HTx7",
        "modelDefName": "company"
      },
      {
        "_id": "fo3hDrzWLBphjpc8m",
        "name": "category",
        "type": "combo",
        "label": "Category",
        "defaultValue": "",
        "hide": false,
        "isReadOnly": false,
        "model_name": "catagory",
        "display_value": "name",
        "option_value": "",
        "multiple": false,
        "modelDefId": "x5gfvTGY83Ha8HTx7",
        "modelDefName": "company"
      }
    ]
  }
];

export const modelDefsDataForAntDTable = fixBusinessKeysForAntDTableRepresentation(modelDefsData);
