export const types = [
  {"_id": "pDso44M65eRH7zPrF", "name": "string"},
  {
    "_id": "fPf7GeEcTKsF8Txhz",
    "name": "text"
  },
  {"_id": "BA6gEjyKBELpLWxci", "name": "date"},
  {
    "_id": "gJmEBzBC4CqvhFjDC",
    "name": "boolean"
  },
  {"_id": "aNFZTZRsJzDNDCuj6", "name": "combo"},
  {
    "_id": "JmNJQHB2xJLTHoKQW",
    "name": "drop_down"
  },
  {"_id": "icsXNe9DFsq34N9eL", "name": "password"},
  {
    "_id": "KREKbzuBpKfuQpPa8",
    "name": "int"
  },
  {"_id": "C63ePtqZrSFG38dSr", "name": "float"},
  {
    "_id": "8F9B8JZjaTZ4q9727",
    "name": "datatable"
  },
  {"_id": "YbF7KFSmGPQPCiAnD", "name": "derived"}
];
