export const modelDefs = [
  {
    "_id": "PqDywsjCT8PiMxtQF",
    "name": "modeldefs",
    "ownerId": "a@a.com",
    "showTableOperation": "false",
    "isBaseModel": "true",
    "businessKeys": [
      "name"
    ],
    "fields": [
      {
        "_id": "uqwf38fGENJS8C8LP",
        "name": "name",
        "type": "string",
        "label": "Name",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "HzWPJstYue57abBo2",
        "name": "_id",
        "type": "string",
        "label": "Id",
        "isReadOnly": "true",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "Zca7ZHj4TBgXGzqCR",
        "name": "showTableOperation",
        "type": "boolean",
        "label": "Show Table Operation",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "wxbgpJCrxuCHc9tN2",
        "name": "isBaseModel",
        "type": "boolean",
        "label": "Is Base Model",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "2Ji5W4NkERQMRGjpm",
        "name": "ownerId",
        "type": "combo",
        "model_name": "user",
        "display_value": "user_name",
        "option_value": "_id",
        "label": "Owner Id",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      },
      {
        "_id": "hMKHMcP7nE6XNughl",
        "name": "businessKeys",
        "type": "string",
        "label": "Business Keys",
        "defaultValue": "",
        "modelDefId": "PqDywsjCT8PiMxtQF",
        "modelDefName": "modeldefs",
        "isReadOnly": "false",
        "hide": "false",
        "isShownInTable": "false"
      }
    ],
    "summaryProperty": ""
  }

]
