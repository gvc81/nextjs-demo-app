import * as _ from "lodash";
import {
  convertBusinessKeysToArray,
  fixBusinessKeysForAntDTableRepresentation,
  fixEmptyBusinessKeys,
  fixStringBusinessKeys
} from "../src/utils/ModelDefDataUtils";

import {
  ModelDefCorrectFixture,
  ModelDefEmptyArrayBusinessKeysFixture,
  ModelDefEmptyStringBusinessKeysFixture,
  ModelDefStringCommaSeparatedBusinessKeysFixture
} from "./fixtures/ModelDefFiture";

describe("ModelDef DataUtils",()=>{
  test('ModelDef Data Utils: fixEmptyBusinessKeys', () => {
    const result = fixEmptyBusinessKeys(ModelDefEmptyStringBusinessKeysFixture);
    expect(result).toEqual(ModelDefEmptyArrayBusinessKeysFixture);
  });

  test('fixStringBusinessKeys', () => {
    const result = fixStringBusinessKeys(ModelDefStringCommaSeparatedBusinessKeysFixture);
    expect(result).toEqual(ModelDefCorrectFixture);
  });

  test('convertBusinessKeysToArray', () => {
    const result = convertBusinessKeysToArray([ModelDefStringCommaSeparatedBusinessKeysFixture]);
    const {businessKeys} = result[0];
    expect(_.isArray(businessKeys)).toEqual(true);
  });

  test('fixBusinessKeysForAntDTableRepresentation', () => {
    const result = fixBusinessKeysForAntDTableRepresentation([ModelDefCorrectFixture]);
    const {businessKeys} = result[0];
    expect(_.isString(businessKeys)).toEqual(true);
    expect(businessKeys.includes(",")).toEqual(true);
  });
});

