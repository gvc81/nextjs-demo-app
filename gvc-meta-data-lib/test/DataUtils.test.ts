import {booleanToStringBoolean, createEmptyValue, NOT_AVAILABLE} from "../src/utils/DataUtils";
import {
  ModelDefCorrectFixture,
  ModelDefEmptyArrayBusinessKeysFixture,
  ModelDefEmptyStringBusinessKeysFixture,
  ModelDefSingleValueStringBusinessKeysFixture
} from "./fixtures/ModelDefFiture";
import {businessKeysPropertyName} from "../src/utils/ModelDefDataUtils";
import {BOOLEAN_FALSE, BOOLEAN_TRUE, STRING_BOOLEAN_FALSE, STRING_BOOLEAN_TRUE} from "./fixtures/DataUtilsFiture";

describe("DataUtils",()=>{
  test('createEmptyValue from empty string', () => {
    const result = createEmptyValue(ModelDefEmptyStringBusinessKeysFixture,businessKeysPropertyName);
    const resultPropertyValue = result[businessKeysPropertyName];
    expect(resultPropertyValue === NOT_AVAILABLE).toEqual(true);
  });

  test('createEmptyValue from non empty string', () => {
    const result = createEmptyValue(ModelDefSingleValueStringBusinessKeysFixture,businessKeysPropertyName);
    const resultPropertyValue = result[businessKeysPropertyName];
    expect(resultPropertyValue !== NOT_AVAILABLE).toEqual(true);
    expect(result).toEqual(ModelDefSingleValueStringBusinessKeysFixture);
  });

  test('createEmptyValue from empty array', () => {
    const result = createEmptyValue(ModelDefEmptyArrayBusinessKeysFixture,businessKeysPropertyName);
    const resultPropertyValue = result[businessKeysPropertyName];
    expect(resultPropertyValue === NOT_AVAILABLE).toEqual(true);
  });

  test('createEmptyValue from non empty array', () => {
    const result = createEmptyValue(ModelDefCorrectFixture,businessKeysPropertyName);
    const resultPropertyValue = result[businessKeysPropertyName];
    expect(resultPropertyValue !== NOT_AVAILABLE).toEqual(true);
    expect(result).toEqual(ModelDefCorrectFixture);
  });

  describe('booleanToStringBoolean', () => {
    test("should convert false value to 'false'",()=>{
      const result = booleanToStringBoolean(BOOLEAN_FALSE);
      expect(result).toEqual(STRING_BOOLEAN_FALSE);
    });

    test("should convert true value to 'true'",()=>{
      const result = booleanToStringBoolean(BOOLEAN_TRUE);
      expect(result).toEqual(STRING_BOOLEAN_TRUE);
    });

    test("should return original value for non boolean, here 'true'",()=>{
      const result = booleanToStringBoolean(STRING_BOOLEAN_TRUE);
      expect(result).toEqual(STRING_BOOLEAN_TRUE);
    });

  });
});
