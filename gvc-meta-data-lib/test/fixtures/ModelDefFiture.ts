export const ModelDefSingleValueStringBusinessKeysFixture = {
  "businessKeys": "firstName"
};

export const ModelDefStringCommaSeparatedBusinessKeysFixture = {
  "businessKeys": "firstName,lastName"
};

export const ModelDefArrayStringBusinessKeysFixture = {
  "businessKeys": "[firstName,lastName]"
};

export const ModelDefEmptyStringBusinessKeysFixture = {
  "businessKeys": ""
};

export const ModelDefEmptyArrayBusinessKeysFixture = {
  "businessKeys": []
};

export const ModelDefCorrectFixture = {
  "businessKeys": [
    "firstName",
    "lastName"
  ],
};
