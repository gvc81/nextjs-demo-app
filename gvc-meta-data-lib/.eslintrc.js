module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:import/typescript"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "plugins": [
    "@typescript-eslint",
    "import"
  ],
  "settings": {
    "import/extensions": [
      ".js",
      ".ts"
    ],
    "import/external-module-folders": [
      "node_modules"
    ]
  },
  "rules": {
    "no-unused-vars": [
      "off"
    ],
    "indent": [
      "error",
      2
    ],
    "quotes": [
      "error",
      "double",
      { "allowTemplateLiterals": true }
    ],
    "semi": [
      "error",
      "always"
    ],
    /* Architectural rules*/
    "import/no-restricted-paths": [
      "error",
      {
        /* Application core code should not use infrastructure or presentation code*/
        "zones": [
          {target: "./src/application-core", from: "./src/infrastructure"},
          {target: "./src/application-core", from: "./src/presentation"}
        ]
      }
    ]
  }
};
